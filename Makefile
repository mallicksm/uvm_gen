################################################################################
# Makefile to generate and simulate UVM Testbench
# Create: 2/22/18
# make sim CMD_ARGS=+UVM_TESTNAME=alt_test
################################################################################
# Global Variables
GIT_ROOT:=$$(git rev-parse --show-toplevel)
TEMPLATE_FILES:=$(subst common.tpl,,$(wildcard *.tpl))
PROJECT:=$$(${GIT_ROOT}/bin/uvm_gen -x project)

################################################################################
# GUI variable to bring up Simvision GUI
GUI?=0
CMD_ARGS?=
SIM_ARGS_LINEDEBUG= -linedebug -nowarn BADPRF
SIM_ARGS_COVERAGE=  -cov_cgsample
SIM_ARGS_GUI= -gui
SIM_ARGS_SVCF= -simvisargs "-input simvision.svcf"
SIM_ARGS_PROBE= -input probe.tcl

SIM_ARGS+=                       \
           ${SIM_ARGS_PROBE}     \
           ${SIM_ARGS_SVCF}      \
           ${SIM_ARGS_GUI}       \
           ${SIM_ARGS_COVERAGE}  \
           ${SIM_ARGS_LINEDEBUG} \

SIM_ARGS= +UVM_OBJECTION_TRACE +UVM_VERBOSITY=UVM_MEDIUM
SIM_ARGS+= ${SIM_ARGS_COVERAGE}

export SIM_ARGS GUI

################################################################################
# Targets
all: gen sim

gen:
	@${GIT_ROOT}/bin/uvm_gen -t ${TEMPLATE_FILES} -p toplevel
	@if [[ -e template.svcf ]]; then cp template.svcf ${PROJECT}/sim/simvision.svcf; fi
	@cp probe.tcl ${PROJECT}/sim/probe.tcl
	@cp -R ${GIT_ROOT}/syosil/ ${PROJECT}/syosil

makefixgen:
	@make -s fixgen

fixgen:
	@echo "Running $@"
	@sed -i 's/.*m_I2C_slave_env_seq.start.*/m_seq_count = m_seq_count; \/\/ make nop\n/' ${PROJECT}/tb/toplevel/sv/toplevel_seq_lib.sv

sim:
	@cd ${PROJECT}/sim; \
	make -s sim CMD_ARGS=+UVM_TESTNAME=alt_test GUI=1

simclean:
	@cd ${PROJECT}/sim; \
	make -s clean

clean:
	@rm -rf ${PROJECT} ${PROJECT}.bak uvm_gen.log .simvision
