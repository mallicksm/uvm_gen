database -open -shm -into waves.shm waves -default
probe -create -database waves toplevel_th -all -depth all
probe -create -database waves toplevel_tb -all -depth all -dynamic
probe -create -database waves $uvm:{uvm_test_top} -all -depth all -dynamic
