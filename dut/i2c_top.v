module i2c_top
(
    input RESETN,//async reset
    input CKREF, //oversample clock

    input [6:0] slave_addr,

    input SCL_IN,
    input SDA_IN,
    output SDA_OE //active high
);

wire sda_oen;
assign SDA_OE = ~sda_oen;

wire psel;
wire penable;
wire pwrite;
wire [7:0] paddr;
wire [7:0] pwdata;
wire [7:0] prdata;
wire pready;
reg [1:0] rstn_sync;
always@(posedge CKREF or negedge RESETN)
  if(~RESETN)
        rstn_sync <= 2'b0;
  else
        rstn_sync <= {rstn_sync[0], 1'b1};

 ctrl_regs u_ctrl_regs
(
    .rstn        (rstn_sync[1]), //async reset
    .clk         (CKREF       ), //oversample clock

    .psel        (psel        ),
    .penable     (penable     ),
    .pwrite      (pwrite      ),
    .paddr       (paddr       ),
    .pwdata      (pwdata      ),

    .read_reg_0  (8'hf0),
    .read_reg_1  (8'hf1),
    .read_reg_2  (8'hf2),
    .read_reg_3  (8'hf3),
    .read_reg_4  (8'hf4),
    .read_reg_5  (8'hf5),
    .read_reg_6  (8'hf6),
    .read_reg_7  (8'hf7),
    .read_reg_8  (8'hf8),
    .read_reg_9  (8'hf9),
    .read_reg_10 (8'hfa),
    .read_reg_11 (8'hfb),
    .read_reg_12 (8'hfc),
    .read_reg_13 (8'hfd),
    .read_reg_14 (8'hfe),
    .read_reg_15 (8'hff),

    .write_reg_default_in_0 (8'h00),
    .write_reg_default_in_1 (8'h01),
    .write_reg_default_in_2 (8'h02),
    .write_reg_default_in_3 (8'h03),
    .write_reg_default_in_4 (8'h04),
    .write_reg_default_in_5 (8'h05),
    .write_reg_default_in_6 (8'h06),
    .write_reg_default_in_7 (8'h07),
    .write_reg_default_in_8 (8'h08),
    .write_reg_default_in_9 (8'h09),
    .write_reg_default_in_10(8'h0a),
    .write_reg_default_in_11(8'h0b),
    .write_reg_default_in_12(8'h0c),
    .write_reg_default_in_13(8'h0d),
    .write_reg_default_in_14(8'h0e),
    .write_reg_default_in_15(8'h0f),
    .write_reg_default_in_16(8'h10),
    .write_reg_default_in_17(8'h11),
    .write_reg_default_in_18(8'h12),
    .write_reg_default_in_19(8'h13),
    .write_reg_default_in_20(8'h14),
    .write_reg_default_in_21(8'h15),
    .write_reg_default_in_22(8'h16),
    .write_reg_default_in_23(8'h17),
    .write_reg_default_in_24(8'h18),
    .write_reg_default_in_25(8'h19),
    .write_reg_default_in_26(8'h1a),
    .write_reg_default_in_27(8'h1b),
    .write_reg_default_in_28(8'h1c),
    .write_reg_default_in_29(8'h1d),
    .write_reg_default_in_30(8'h1e),
    .write_reg_default_in_31(8'h1f),
    .write_reg_default_in_32(8'h20),
    .write_reg_default_in_33(8'h21),
    .write_reg_default_in_34(8'h22),
    .write_reg_default_in_35(8'h23),
    .write_reg_default_in_36(8'h24),
    .write_reg_default_in_37(8'h25),
    .write_reg_default_in_38(8'h26),
    .write_reg_default_in_39(8'h27),
    .write_reg_default_in_40(8'h28),
    .write_reg_default_in_41(8'h29),
    .write_reg_default_in_42(8'h2a),
    .write_reg_default_in_43(8'h2b),
    .write_reg_default_in_44(8'h2c),
    .write_reg_default_in_45(8'h2d),
    .write_reg_default_in_46(8'h2e),
    .write_reg_default_in_47(8'h2f),

    .prdata      (prdata      ),
    .pready      (pready      )
);

i2c_slave u_i2c_slave
(
    .rstn         (rstn_sync[1]),//async reset
    .clk          (CKREF ), //oversample clock

    .slave_addr   (slave_addr),
    .write_protect(1'b0), //active high

    .scl          (SCL_IN),
    .scl_oen      (), //active low;
    .sda          (SDA_IN),
    .sda_oen        (sda_oen), //sda is open drain IO, output always tie to 0

    .read_err     (),
    .psel         (psel         ),
    .penable      (penable      ),
    .pwrite       (pwrite       ),
    .paddr        (paddr        ),
    .pwdata       (pwdata       ),
    .prdata       (prdata       ),
    .pready       (pready       )
);

endmodule
