module ctrl_regs
(
    input rstn,//async reset
    input clk, //oversample clock

    input psel,
    input penable,
    input pwrite,
    input [7:0] paddr,
    input [7:0] pwdata,

    input [7:0] read_reg_0,
    input [7:0] read_reg_1,
    input [7:0] read_reg_2,
    input [7:0] read_reg_3,
    input [7:0] read_reg_4,
    input [7:0] read_reg_5,
    input [7:0] read_reg_6,
    input [7:0] read_reg_7,
    input [7:0] read_reg_8,
    input [7:0] read_reg_9,
    input [7:0] read_reg_10,
    input [7:0] read_reg_11,
    input [7:0] read_reg_12,
    input [7:0] read_reg_13,
    input [7:0] read_reg_14,
    input [7:0] read_reg_15,

    input [7:0] write_reg_default_in_0 ,
    input [7:0] write_reg_default_in_1 ,
    input [7:0] write_reg_default_in_2 ,
    input [7:0] write_reg_default_in_3 ,
    input [7:0] write_reg_default_in_4 ,
    input [7:0] write_reg_default_in_5 ,
    input [7:0] write_reg_default_in_6 ,
    input [7:0] write_reg_default_in_7 ,
    input [7:0] write_reg_default_in_8 ,
    input [7:0] write_reg_default_in_9 ,
    input [7:0] write_reg_default_in_10,
    input [7:0] write_reg_default_in_11,
    input [7:0] write_reg_default_in_12,
    input [7:0] write_reg_default_in_13,
    input [7:0] write_reg_default_in_14,
    input [7:0] write_reg_default_in_15,
    input [7:0] write_reg_default_in_16,
    input [7:0] write_reg_default_in_17,
    input [7:0] write_reg_default_in_18,
    input [7:0] write_reg_default_in_19,
    input [7:0] write_reg_default_in_20,
    input [7:0] write_reg_default_in_21,
    input [7:0] write_reg_default_in_22,
    input [7:0] write_reg_default_in_23,
    input [7:0] write_reg_default_in_24,
    input [7:0] write_reg_default_in_25,
    input [7:0] write_reg_default_in_26,
    input [7:0] write_reg_default_in_27,
    input [7:0] write_reg_default_in_28,
    input [7:0] write_reg_default_in_29,
    input [7:0] write_reg_default_in_30,
    input [7:0] write_reg_default_in_31,
    input [7:0] write_reg_default_in_32,
    input [7:0] write_reg_default_in_33,
    input [7:0] write_reg_default_in_34,
    input [7:0] write_reg_default_in_35,
    input [7:0] write_reg_default_in_36,
    input [7:0] write_reg_default_in_37,
    input [7:0] write_reg_default_in_38,
    input [7:0] write_reg_default_in_39,
    input [7:0] write_reg_default_in_40,
    input [7:0] write_reg_default_in_41,
    input [7:0] write_reg_default_in_42,
    input [7:0] write_reg_default_in_43,
    input [7:0] write_reg_default_in_44,
    input [7:0] write_reg_default_in_45,
    input [7:0] write_reg_default_in_46,
    input [7:0] write_reg_default_in_47,

    output  [7:0] write_reg_0,
    output  [7:0] write_reg_1,
    output  [7:0] write_reg_2,
    output  [7:0] write_reg_3,
    output  [7:0] write_reg_4,
    output  [7:0] write_reg_5,
    output  [7:0] write_reg_6,
    output  [7:0] write_reg_7,
    output  [7:0] write_reg_8,
    output  [7:0] write_reg_9,
    output  [7:0] write_reg_10,
    output  [7:0] write_reg_11,
    output  [7:0] write_reg_12,
    output  [7:0] write_reg_13,
    output  [7:0] write_reg_14,
    output  [7:0] write_reg_15,
    output  [7:0] write_reg_16,
    output  [7:0] write_reg_17,
    output  [7:0] write_reg_18,
    output  [7:0] write_reg_19,
    output  [7:0] write_reg_20,
    output  [7:0] write_reg_21,
    output  [7:0] write_reg_22,
    output  [7:0] write_reg_23,
    output  [7:0] write_reg_24,
    output  [7:0] write_reg_25,
    output  [7:0] write_reg_26,
    output  [7:0] write_reg_27,
    output  [7:0] write_reg_28,
    output  [7:0] write_reg_29,
    output  [7:0] write_reg_30,
    output  [7:0] write_reg_31,

    output  [7:0] write_reg_32,
    output  [7:0] write_reg_33,
    output  [7:0] write_reg_34,
    output  [7:0] write_reg_35,
    output  [7:0] write_reg_36,
    output  [7:0] write_reg_37,
    output  [7:0] write_reg_38,
    output  [7:0] write_reg_39,
    output  [7:0] write_reg_40,
    output  [7:0] write_reg_41,
    output  [7:0] write_reg_42,
    output  [7:0] write_reg_43,
    output  [7:0] write_reg_44,
    output  [7:0] write_reg_45,
    output  [7:0] write_reg_46,
    output  [7:0] write_reg_47,

    output reg [7:0] prdata,
    output pready
);

reg [15:0] rstn_dly;
reg        srstn;

reg [7:0] write_reg_0_pre;
reg [7:0] write_reg_1_pre;
reg [7:0] write_reg_2_pre;
reg [7:0] write_reg_3_pre;
reg [7:0] write_reg_4_pre;
reg [7:0] write_reg_5_pre;
reg [7:0] write_reg_6_pre;
reg [7:0] write_reg_7_pre;
reg [7:0] write_reg_8_pre;
reg [7:0] write_reg_9_pre;
reg [7:0] write_reg_10_pre;
reg [7:0] write_reg_11_pre;
reg [7:0] write_reg_12_pre;
reg [7:0] write_reg_13_pre;
reg [7:0] write_reg_14_pre;
reg [7:0] write_reg_15_pre;
reg [7:0] write_reg_16_pre;
reg [7:0] write_reg_17_pre;
reg [7:0] write_reg_18_pre;
reg [7:0] write_reg_19_pre;
reg [7:0] write_reg_20_pre;
reg [7:0] write_reg_21_pre;
reg [7:0] write_reg_22_pre;
reg [7:0] write_reg_23_pre;
reg [7:0] write_reg_24_pre;
reg [7:0] write_reg_25_pre;
reg [7:0] write_reg_26_pre;
reg [7:0] write_reg_27_pre;
reg [7:0] write_reg_28_pre;
reg [7:0] write_reg_29_pre;
reg [7:0] write_reg_30_pre;
reg [7:0] write_reg_31_pre;
reg [7:0] write_reg_32_pre;
reg [7:0] write_reg_33_pre;
reg [7:0] write_reg_34_pre;
reg [7:0] write_reg_35_pre;
reg [7:0] write_reg_36_pre;
reg [7:0] write_reg_37_pre;
reg [7:0] write_reg_38_pre;
reg [7:0] write_reg_39_pre;
reg [7:0] write_reg_40_pre;
reg [7:0] write_reg_41_pre;
reg [7:0] write_reg_42_pre;
reg [7:0] write_reg_43_pre;
reg [7:0] write_reg_44_pre;
reg [7:0] write_reg_45_pre;
reg [7:0] write_reg_46_pre;
reg [7:0] write_reg_47_pre;


assign write_reg_0  = rstn_dly[15] ? write_reg_0_pre : write_reg_default_in_0;
assign write_reg_1  = rstn_dly[15] ? write_reg_1_pre : write_reg_default_in_1;
assign write_reg_2  = rstn_dly[15] ? write_reg_2_pre : write_reg_default_in_2;
assign write_reg_3  = rstn_dly[15] ? write_reg_3_pre : write_reg_default_in_3;
assign write_reg_4  = rstn_dly[15] ? write_reg_4_pre : write_reg_default_in_4;
assign write_reg_5  = rstn_dly[15] ? write_reg_5_pre : write_reg_default_in_5;
assign write_reg_6  = rstn_dly[15] ? write_reg_6_pre : write_reg_default_in_6;
assign write_reg_7  = rstn_dly[15] ? write_reg_7_pre : write_reg_default_in_7;
assign write_reg_8  = rstn_dly[15] ? write_reg_8_pre : write_reg_default_in_8;
assign write_reg_9  = rstn_dly[15] ? write_reg_9_pre : write_reg_default_in_9;
assign write_reg_10 = rstn_dly[15] ? write_reg_10_pre : write_reg_default_in_10;
assign write_reg_11 = rstn_dly[15] ? write_reg_11_pre : write_reg_default_in_11;
assign write_reg_12 = rstn_dly[15] ? write_reg_12_pre : write_reg_default_in_12;
assign write_reg_13 = rstn_dly[15] ? write_reg_13_pre : write_reg_default_in_13;
assign write_reg_14 = rstn_dly[15] ? write_reg_14_pre : write_reg_default_in_14;
assign write_reg_15 = rstn_dly[15] ? write_reg_15_pre : write_reg_default_in_15;
assign write_reg_16 = rstn_dly[15] ? write_reg_16_pre : write_reg_default_in_16;
assign write_reg_17 = rstn_dly[15] ? write_reg_17_pre : write_reg_default_in_17;
assign write_reg_18 = rstn_dly[15] ? write_reg_18_pre : write_reg_default_in_18;
assign write_reg_19 = rstn_dly[15] ? write_reg_19_pre : write_reg_default_in_19;
assign write_reg_20 = rstn_dly[15] ? write_reg_20_pre : write_reg_default_in_20;
assign write_reg_21 = rstn_dly[15] ? write_reg_21_pre : write_reg_default_in_21;
assign write_reg_22 = rstn_dly[15] ? write_reg_22_pre : write_reg_default_in_22;
assign write_reg_23 = rstn_dly[15] ? write_reg_23_pre : write_reg_default_in_23;
assign write_reg_24 = rstn_dly[15] ? write_reg_24_pre : write_reg_default_in_24;
assign write_reg_25 = rstn_dly[15] ? write_reg_25_pre : write_reg_default_in_25;
assign write_reg_26 = rstn_dly[15] ? write_reg_26_pre : write_reg_default_in_26;
assign write_reg_27 = rstn_dly[15] ? write_reg_27_pre : write_reg_default_in_27;
assign write_reg_28 = rstn_dly[15] ? write_reg_28_pre : write_reg_default_in_28;
assign write_reg_29 = rstn_dly[15] ? write_reg_29_pre : write_reg_default_in_29;
assign write_reg_30 = rstn_dly[15] ? write_reg_30_pre : write_reg_default_in_30;
assign write_reg_31 = rstn_dly[15] ? write_reg_31_pre : write_reg_default_in_31;
assign write_reg_32 = rstn_dly[15] ? write_reg_32_pre : write_reg_default_in_32;
assign write_reg_33 = rstn_dly[15] ? write_reg_33_pre : write_reg_default_in_33;
assign write_reg_34 = rstn_dly[15] ? write_reg_34_pre : write_reg_default_in_34;
assign write_reg_35 = rstn_dly[15] ? write_reg_35_pre : write_reg_default_in_35;
assign write_reg_36 = rstn_dly[15] ? write_reg_36_pre : write_reg_default_in_36;
assign write_reg_37 = rstn_dly[15] ? write_reg_37_pre : write_reg_default_in_37;
assign write_reg_38 = rstn_dly[15] ? write_reg_38_pre : write_reg_default_in_38;
assign write_reg_39 = rstn_dly[15] ? write_reg_39_pre : write_reg_default_in_39;
assign write_reg_40 = rstn_dly[15] ? write_reg_40_pre : write_reg_default_in_40;
assign write_reg_41 = rstn_dly[15] ? write_reg_41_pre : write_reg_default_in_41;
assign write_reg_42 = rstn_dly[15] ? write_reg_42_pre : write_reg_default_in_42;
assign write_reg_43 = rstn_dly[15] ? write_reg_43_pre : write_reg_default_in_43;
assign write_reg_44 = rstn_dly[15] ? write_reg_44_pre : write_reg_default_in_44;
assign write_reg_45 = rstn_dly[15] ? write_reg_45_pre : write_reg_default_in_45;
assign write_reg_46 = rstn_dly[15] ? write_reg_46_pre : write_reg_default_in_46;
assign write_reg_47 = rstn_dly[15] ? write_reg_47_pre : write_reg_default_in_47;

assign pready = 1'b1;

always@(posedge clk or negedge rstn)
   if(!rstn)
      srstn <= 1'b0;
   else
      srstn <= 1'b1;

always@(posedge clk)rstn_dly[15:0]<={rstn_dly[14:0],srstn};

always@(posedge clk or negedge rstn)
    if(!rstn)
        prdata <= 8'h00;
    else if(psel & ~penable & ~pwrite)
    begin
      casez(paddr[7:0])
        8'h00 : prdata <= write_reg_0_pre;
        8'h01 : prdata <= write_reg_1_pre;
        8'h02 : prdata <= write_reg_2_pre;
        8'h03 : prdata <= write_reg_3_pre;
        8'h04 : prdata <= write_reg_4_pre;
        8'h05 : prdata <= write_reg_5_pre;
        8'h06 : prdata <= write_reg_6_pre;
        8'h07 : prdata <= write_reg_7_pre;
        8'h08 : prdata <= write_reg_8_pre;
        8'h09 : prdata <= write_reg_9_pre;
        8'h0a : prdata <= write_reg_10_pre;
        8'h0b : prdata <= write_reg_11_pre;
        8'h0c : prdata <= write_reg_12_pre;
        8'h0d : prdata <= write_reg_13_pre;
        8'h0e : prdata <= write_reg_14_pre;
        8'h0f : prdata <= write_reg_15_pre;
        8'h10 : prdata <= write_reg_16_pre;
        8'h11 : prdata <= write_reg_17_pre;
        8'h12 : prdata <= write_reg_18_pre;
        8'h13 : prdata <= write_reg_19_pre;
        8'h14 : prdata <= write_reg_20_pre;
        8'h15 : prdata <= write_reg_21_pre;
        8'h16 : prdata <= write_reg_22_pre;
        8'h17 : prdata <= write_reg_23_pre;
        8'h18 : prdata <= write_reg_24_pre;
        8'h19 : prdata <= write_reg_25_pre;
        8'h1a : prdata <= write_reg_26_pre;
        8'h1b : prdata <= write_reg_27_pre;
        8'h1c : prdata <= write_reg_28_pre;
        8'h1d : prdata <= write_reg_29_pre;
        8'h1e : prdata <= write_reg_30_pre;
        8'h1f : prdata <= write_reg_31_pre;
        8'h20 : prdata <= write_reg_32_pre;
        8'h21 : prdata <= write_reg_33_pre;
        8'h22 : prdata <= write_reg_34_pre;
        8'h23 : prdata <= write_reg_35_pre;
        8'h24 : prdata <= write_reg_36_pre;
        8'h25 : prdata <= write_reg_37_pre;
        8'h26 : prdata <= write_reg_38_pre;
        8'h27 : prdata <= write_reg_39_pre;
        8'h28 : prdata <= write_reg_40_pre;
        8'h29 : prdata <= write_reg_41_pre;
        8'h2a : prdata <= write_reg_42_pre;
        8'h2b : prdata <= write_reg_43_pre;
        8'h2c : prdata <= write_reg_44_pre;
        8'h2d : prdata <= write_reg_45_pre;
        8'h2e : prdata <= write_reg_46_pre;
        8'h2f : prdata <= write_reg_47_pre;

        8'h40 : prdata <= read_reg_0 ;
        8'h41 : prdata <= read_reg_1 ;
        8'h42 : prdata <= read_reg_2 ;
        8'h43 : prdata <= read_reg_3 ;
        8'h44 : prdata <= read_reg_4 ;
        8'h45 : prdata <= read_reg_5 ;
        8'h46 : prdata <= read_reg_6 ;
        8'h47 : prdata <= read_reg_7 ;
        8'h48 : prdata <= read_reg_8 ;
        8'h49 : prdata <= read_reg_9 ;
        8'h4a : prdata <= read_reg_10;
        8'h4b : prdata <= read_reg_11;
        8'h4c : prdata <= read_reg_12;
        8'h4d : prdata <= read_reg_13;
        8'h4e : prdata <= read_reg_14;
        8'h4f : prdata <= read_reg_15;
      endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn)begin
        write_reg_0_pre  <= 8'h0;
        write_reg_1_pre  <= 8'h0;
        write_reg_2_pre  <= 8'h0;
        write_reg_3_pre  <= 8'h0;
        write_reg_4_pre  <= 8'h0;
        write_reg_5_pre  <= 8'h0;
        write_reg_6_pre  <= 8'h0;
        write_reg_7_pre  <= 8'h0;
        write_reg_8_pre  <= 8'h0;
        write_reg_9_pre  <= 8'h0;
        write_reg_10_pre <= 8'h0;
        write_reg_11_pre <= 8'h0;
        write_reg_12_pre <= 8'h0;
        write_reg_13_pre <= 8'h0;
        write_reg_14_pre <= 8'h0;
        write_reg_15_pre <= 8'h0;
        write_reg_16_pre <= 8'h0;
        write_reg_17_pre <= 8'h0;
        write_reg_18_pre <= 8'h0;
        write_reg_19_pre <= 8'h0;
        write_reg_20_pre <= 8'h0;
        write_reg_21_pre <= 8'h0;
        write_reg_22_pre <= 8'h0;
        write_reg_23_pre <= 8'h0;
        write_reg_24_pre <= 8'h0;
        write_reg_25_pre <= 8'h0;
        write_reg_26_pre <= 8'h0;
        write_reg_27_pre <= 8'h0;
        write_reg_28_pre <= 8'h0;
        write_reg_29_pre <= 8'h0;
        write_reg_30_pre <= 8'h0;
        write_reg_31_pre <= 8'h0;
        write_reg_32_pre <= 8'h0;
        write_reg_33_pre <= 8'h0;
        write_reg_34_pre <= 8'h0;
        write_reg_35_pre <= 8'h0;
        write_reg_36_pre <= 8'h0;
        write_reg_37_pre <= 8'h0;
        write_reg_38_pre <= 8'h0;
        write_reg_39_pre <= 8'h0;
        write_reg_40_pre <= 8'h0;
        write_reg_41_pre <= 8'h0;
        write_reg_42_pre <= 8'h0;
        write_reg_43_pre <= 8'h0;
        write_reg_44_pre <= 8'h0;
        write_reg_45_pre <= 8'h0;
        write_reg_46_pre <= 8'h0;
        write_reg_47_pre <= 8'h0;
    end
    else if(rstn_dly[11] & ~rstn_dly[12])begin
        write_reg_0_pre  <= write_reg_default_in_0 ;
        write_reg_1_pre  <= write_reg_default_in_1 ;
        write_reg_2_pre  <= write_reg_default_in_2 ;
        write_reg_3_pre  <= write_reg_default_in_3 ;
        write_reg_4_pre  <= write_reg_default_in_4 ;
        write_reg_5_pre  <= write_reg_default_in_5 ;
        write_reg_6_pre  <= write_reg_default_in_6 ;
        write_reg_7_pre  <= write_reg_default_in_7 ;
        write_reg_8_pre  <= write_reg_default_in_8 ;
        write_reg_9_pre  <= write_reg_default_in_9 ;
        write_reg_10_pre <= write_reg_default_in_10;
        write_reg_11_pre <= write_reg_default_in_11;
        write_reg_12_pre <= write_reg_default_in_12;
        write_reg_13_pre <= write_reg_default_in_13;
        write_reg_14_pre <= write_reg_default_in_14;
        write_reg_15_pre <= write_reg_default_in_15;
        write_reg_16_pre <= write_reg_default_in_16;
        write_reg_17_pre <= write_reg_default_in_17;
        write_reg_18_pre <= write_reg_default_in_18;
        write_reg_19_pre <= write_reg_default_in_19;
        write_reg_20_pre <= write_reg_default_in_20;
        write_reg_21_pre <= write_reg_default_in_21;
        write_reg_22_pre <= write_reg_default_in_22;
        write_reg_23_pre <= write_reg_default_in_23;
        write_reg_24_pre <= write_reg_default_in_24;
        write_reg_25_pre <= write_reg_default_in_25;
        write_reg_26_pre <= write_reg_default_in_26;
        write_reg_27_pre <= write_reg_default_in_27;
        write_reg_28_pre <= write_reg_default_in_28;
        write_reg_29_pre <= write_reg_default_in_29;
        write_reg_30_pre <= write_reg_default_in_30;
        write_reg_31_pre <= write_reg_default_in_31;
        write_reg_32_pre <= write_reg_default_in_32;
        write_reg_33_pre <= write_reg_default_in_33;
        write_reg_34_pre <= write_reg_default_in_34;
        write_reg_35_pre <= write_reg_default_in_35;
        write_reg_36_pre <= write_reg_default_in_36;
        write_reg_37_pre <= write_reg_default_in_37;
        write_reg_38_pre <= write_reg_default_in_38;
        write_reg_39_pre <= write_reg_default_in_39;
        write_reg_40_pre <= write_reg_default_in_40;
        write_reg_41_pre <= write_reg_default_in_41;
        write_reg_42_pre <= write_reg_default_in_42;
        write_reg_43_pre <= write_reg_default_in_43;
        write_reg_44_pre <= write_reg_default_in_44;
        write_reg_45_pre <= write_reg_default_in_45;
        write_reg_46_pre <= write_reg_default_in_46;
        write_reg_47_pre <= write_reg_default_in_47;
    end
    else begin
        if(psel & penable & pwrite & paddr==8'h00)write_reg_0_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h01)write_reg_1_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h02)write_reg_2_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h03)write_reg_3_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h04)write_reg_4_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h05)write_reg_5_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h06)write_reg_6_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h07)write_reg_7_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h08)write_reg_8_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h09)write_reg_9_pre  <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0a)write_reg_10_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0b)write_reg_11_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0c)write_reg_12_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0d)write_reg_13_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0e)write_reg_14_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h0f)write_reg_15_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h10)write_reg_16_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h11)write_reg_17_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h12)write_reg_18_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h13)write_reg_19_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h14)write_reg_20_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h15)write_reg_21_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h16)write_reg_22_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h17)write_reg_23_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h18)write_reg_24_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h19)write_reg_25_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1a)write_reg_26_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1b)write_reg_27_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1c)write_reg_28_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1d)write_reg_29_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1e)write_reg_30_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h1f)write_reg_31_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h20)write_reg_32_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h21)write_reg_33_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h22)write_reg_34_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h23)write_reg_35_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h24)write_reg_36_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h25)write_reg_37_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h26)write_reg_38_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h27)write_reg_39_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h28)write_reg_40_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h29)write_reg_41_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2a)write_reg_42_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2b)write_reg_43_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2c)write_reg_44_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2d)write_reg_45_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2e)write_reg_46_pre <= pwdata[7:0];
        if(psel & penable & pwrite & paddr==8'h2f)write_reg_47_pre <= pwdata[7:0];
    end

endmodule
