module AIM915 (/*AUTOARG*/
   // Outputs
   INTB,
   // Inouts
   GPIO_0, GPIO_1, SCL, SDA, DOUT_N, DOUT_P,
   // Inputs
   RXCLKIN_N, RXCLKIN_P, RXIN_0_N, RXIN_0_P, RXIN_1_N, RXIN_1_P,
   RXIN_2_N, RXIN_2_P, RXIN_3_N, RXIN_3_P, I2S_DA, I2S_DB, I2S_DC,
   I2S_DD, I2S_WC, I2S_CLK, BKWD, LFMODE, MAPSEL, REPEAT, IDx, PDB,
   GND, VDD33_A, VDD33_B, VDDIO_A, VDDIO_B, CAPL12, CAPP12, CAPHS12,
   CAPLVD12, RES_0, RES_1
   );

// LVDS INPUTS
input               RXCLKIN_N;
input               RXCLKIN_P;

input               RXIN_0_N;
input               RXIN_0_P;
input               RXIN_1_N;
input               RXIN_1_P;
input               RXIN_2_N;
input               RXIN_2_P;
input               RXIN_3_N;
input               RXIN_3_P;

// LVCMOS GPIO
inout               GPIO_0;
inout               GPIO_1;

// I2S
input               I2S_DA;
input               I2S_DB;
input               I2S_DC;
input               I2S_DD;

input               I2S_WC;
input               I2S_CLK;

// MISC
input               BKWD;
input               LFMODE;
input               MAPSEL;
input               REPEAT;
output              INTB;

input               IDx;
input               PDB;

// I2C
inout               SCL;
inout               SDA;

// AHDL Link 
inout               DOUT_N;
inout               DOUT_P;

// PWR and GND
input               GND;
input               VDD33_A;
input               VDD33_B;
input               VDDIO_A;
input               VDDIO_B;

input               CAPL12;
input               CAPP12;
input               CAPHS12;
input               CAPLVD12;

input               RES_0;
input               RES_1;

/*AUTOWIRE*/

reg CKREF, RESETN;
initial 
CKREF = 0;
always #1 CKREF = ~CKREF;
initial
RESETN = 0;
initial #75 RESETN = 1;

wire oe;
wire [6:0] slave_addr = 7'h50;
i2c_top i_i2c_top(
   .RESETN(RESETN),
   .CKREF(CKREF),
   .slave_addr(slave_addr[6:0]),
   .SCL_IN(SCL),
   .SDA_IN(SDA),
   .SDA_OE(oe)
);

assign SDA = 1'b0 ? 1'b0 : 1'bz;
// Dummy Behavior Function
//assign oe = 1'b1;
assign INTB = 1'b1;
//assign GPIO_0 = oe ? 1'b0 : 1'bz;
//assign GPIO_1 = oe ? 1'b0 : 1'bz;
//assign SCL = oe ? 1'b0 : 1'bz;
//assign DOUT_N = oe ? 1'b0 : 1'bz;
//assign DOUT_P = oe ? 1'b0 : 1'bz;

endmodule
