`timescale 1ns/1ns
`define S_DLY 1000
`define V_DLY 2000
`define C_DLY 3000
`define A_DLY 4000

module dummy_dut_top(/*autoarg*/
   // Outputs
   aim916_TXCLKOUT_N, aim916_TXCLKOUT_P, aim916_TXOUT_0_N,
   aim916_TXOUT_0_P, aim916_TXOUT_1_N, aim916_TXOUT_1_P,
   aim916_TXOUT_2_N, aim916_TXOUT_2_P, aim916_TXOUT_3_N,
   aim916_TXOUT_3_P, aim916_I2S_DA, aim916_I2S_DB, aim916_I2S_DC,
   aim916_I2S_DD, aim916_MCLK, aim916_I2S_WC, aim916_I2S_CLK,
   aim916_LOCK, aim916_PASS, aim916_CMLOUT_N, aim916_CMLOUT_P,
   aim916_DMF,
   // Inouts
   aim915_GPIO_0, aim915_GPIO_1, aim915_SCL, aim915_SDA,
   aim916_GPIO_0, aim916_GPIO_1, aim916_SCL, aim916_SDA,
   // Inputs
   aim915_RXCLKIN_N, aim915_RXCLKIN_P, aim915_RXIN_0_N,
   aim915_RXIN_0_P, aim915_RXIN_1_N, aim915_RXIN_1_P, aim915_RXIN_2_N,
   aim915_RXIN_2_P, aim915_RXIN_3_N, aim915_RXIN_3_P, aim915_I2S_DA,
   aim915_I2S_DB, aim915_I2S_DC, aim915_I2S_DD, aim915_I2S_WC,
   aim915_I2S_CLK, aim915_BKWD, aim915_LFMODE, aim915_MAPSEL,
   aim915_REPEAT, aim915_IDx, aim915_PDB, aim916_BISTEN,
   aim916_LFMODE, aim916_MAPSEL, aim916_MODE_SEL, aim916_OEN,
   aim916_PDB, aim916_IDx
   );


// AIM915 Ports

// LVDS INPUTS
input               aim915_RXCLKIN_N;
input               aim915_RXCLKIN_P;

input               aim915_RXIN_0_N;
input               aim915_RXIN_0_P;
input               aim915_RXIN_1_N;
input               aim915_RXIN_1_P;
input               aim915_RXIN_2_N;
input               aim915_RXIN_2_P;
input               aim915_RXIN_3_N;
input               aim915_RXIN_3_P;

// LVCMOS GPIO
inout               aim915_GPIO_0;
inout               aim915_GPIO_1;

// I2S
input               aim915_I2S_DA;
input               aim915_I2S_DB;
input               aim915_I2S_DC;
input               aim915_I2S_DD;

input               aim915_I2S_WC;
input               aim915_I2S_CLK;

// MISC
input               aim915_BKWD;
input               aim915_LFMODE;
input               aim915_MAPSEL;
input               aim915_REPEAT;
//output              aim915_INTB;

input               aim915_IDx;
input               aim915_PDB;

// I2C
inout               aim915_SCL;
inout               aim915_SDA;

//AIM916 Ports
// LVDS OUTPUTS
output               aim916_TXCLKOUT_N;
output               aim916_TXCLKOUT_P;
output               aim916_TXOUT_0_N;
output               aim916_TXOUT_0_P;
output               aim916_TXOUT_1_N;
output               aim916_TXOUT_1_P;
output               aim916_TXOUT_2_N;
output               aim916_TXOUT_2_P;
output               aim916_TXOUT_3_N;
output               aim916_TXOUT_3_P;

// LVCMOS GPIO
inout                aim916_GPIO_0;
inout                aim916_GPIO_1;

// I2S
output               aim916_I2S_DA;
output               aim916_I2S_DB;
output               aim916_I2S_DC;
output               aim916_I2S_DD;
output               aim916_MCLK;
output               aim916_I2S_WC;
output               aim916_I2S_CLK;

// MISC
//input               aim916_INTB_IN;
input               aim916_BISTEN;
input               aim916_LFMODE;
input               aim916_MAPSEL;
input               aim916_MODE_SEL;
input               aim916_OEN;
input               aim916_PDB;
output              aim916_LOCK;
output              aim916_PASS;
input               aim916_IDx;

// I2C
inout               aim916_SCL;
inout               aim916_SDA;

// AHDL Link 
//inout               aim916_RIN_N;
//inout               aim916_RIN_P;
output              aim916_CMLOUT_N;
output              aim916_CMLOUT_P;
output              aim916_DMF;

pullup (aim915_SCL);
pullup (aim915_SDA);
pullup (aim916_SCL);
pullup (aim916_SDA);

/*AUTOREG*/
// Beginning of automatic regs (for this module's undeclared outputs)
reg			aim916_I2S_CLK;
reg			aim916_I2S_DA;
reg			aim916_I2S_DB;
reg			aim916_I2S_DC;
reg			aim916_I2S_DD;
reg			aim916_I2S_WC;
reg			aim916_MCLK;
reg			aim916_TXCLKOUT_N;
reg			aim916_TXCLKOUT_P;
reg			aim916_TXOUT_0_N;
reg			aim916_TXOUT_0_P;
reg			aim916_TXOUT_1_N;
reg			aim916_TXOUT_1_P;
reg			aim916_TXOUT_2_N;
reg			aim916_TXOUT_2_P;
reg			aim916_TXOUT_3_N;
reg			aim916_TXOUT_3_P;
// End of automatics
/*AUTOWIRE*/
// Beginning of automatic wires (for undeclared instantiated-module outputs)
wire			DOUT_N;			// To/From U_AIM915 of AIM915.v, ...
wire			DOUT_P;			// To/From U_AIM915 of AIM915.v, ...
wire			INTB;			// From U_AIM915 of AIM915.v
// End of automatics
AIM915 U_AIM915 (/*AUTOINST*/
		 // Outputs
		 .INTB			(INTB),			 // Templated
		 // Inouts
		 .GPIO_0		(aim915_GPIO_0),	 // Templated
		 .GPIO_1		(aim915_GPIO_1),	 // Templated
		 .SCL			(aim915_SCL),		 // Templated
		 .SDA			(aim915_SDA),		 // Templated
		 .DOUT_N		(DOUT_N),		 // Templated
		 .DOUT_P		(DOUT_P),		 // Templated
		 // Inputs
		 .RXCLKIN_N		(aim915_RXCLKIN_N),	 // Templated
		 .RXCLKIN_P		(aim915_RXCLKIN_P),	 // Templated
		 .RXIN_0_N		(aim915_RXIN_0_N),	 // Templated
		 .RXIN_0_P		(aim915_RXIN_0_P),	 // Templated
		 .RXIN_1_N		(aim915_RXIN_1_N),	 // Templated
		 .RXIN_1_P		(aim915_RXIN_1_P),	 // Templated
		 .RXIN_2_N		(aim915_RXIN_2_N),	 // Templated
		 .RXIN_2_P		(aim915_RXIN_2_P),	 // Templated
		 .RXIN_3_N		(aim915_RXIN_3_N),	 // Templated
		 .RXIN_3_P		(aim915_RXIN_3_P),	 // Templated
		 .I2S_DA		(aim915_I2S_DA),	 // Templated
		 .I2S_DB		(aim915_I2S_DB),	 // Templated
		 .I2S_DC		(aim915_I2S_DC),	 // Templated
		 .I2S_DD		(aim915_I2S_DD),	 // Templated
		 .I2S_WC		(aim915_I2S_WC),	 // Templated
		 .I2S_CLK		(aim915_I2S_CLK),	 // Templated
		 .BKWD			(aim915_BKWD),		 // Templated
		 .LFMODE		(aim915_LFMODE),	 // Templated
		 .MAPSEL		(aim915_MAPSEL),	 // Templated
		 .REPEAT		(aim915_REPEAT),	 // Templated
		 .IDx			(aim915_IDx),		 // Templated
		 .PDB			(aim915_PDB),		 // Templated
		 .GND			(1'b0),			 // Templated
		 .VDD33_A		(1'b1),			 // Templated
		 .VDD33_B		(1'b1),			 // Templated
		 .VDDIO_A		(1'b1),			 // Templated
		 .VDDIO_B		(1'b1),			 // Templated
		 .CAPL12		(1'b0),			 // Templated
		 .CAPP12		(1'b0),			 // Templated
		 .CAPHS12		(1'b0),			 // Templated
		 .CAPLVD12		(1'b0),			 // Templated
		 .RES_0			(1'b0),			 // Templated
		 .RES_1			(1'b0));			 // Templated

AIM916 U_AIM916 (/*AUTOINST*/
		 // Outputs
		 .TXCLKOUT_N		(),			 // Templated
		 .TXCLKOUT_P		(),			 // Templated
		 .TXOUT_0_N		(),			 // Templated
		 .TXOUT_0_P		(),			 // Templated
		 .TXOUT_1_N		(),			 // Templated
		 .TXOUT_1_P		(),			 // Templated
		 .TXOUT_2_N		(),			 // Templated
		 .TXOUT_2_P		(),			 // Templated
		 .TXOUT_3_N		(),			 // Templated
		 .TXOUT_3_P		(),			 // Templated
		 .I2S_DA		(),			 // Templated
		 .I2S_DB		(),			 // Templated
		 .I2S_DC		(),			 // Templated
		 .I2S_DD		(),			 // Templated
		 .MCLK			(),			 // Templated
		 .I2S_WC		(),			 // Templated
		 .I2S_CLK		(),			 // Templated
		 .LOCK			(aim916_LOCK),		 // Templated
		 .PASS			(aim916_PASS),		 // Templated
		 .CMLOUT_N		(aim916_CMLOUT_N),	 // Templated
		 .CMLOUT_P		(aim916_CMLOUT_P),	 // Templated
		 .DMF			(aim916_DMF),		 // Templated
		 // Inouts
		 .GPIO_0		(aim916_GPIO_0),	 // Templated
		 .GPIO_1		(aim916_GPIO_1),	 // Templated
		 .SCL			(aim916_SCL),		 // Templated
		 .SDA			(aim916_SDA),		 // Templated
		 .RIN_N			(DOUT_N),		 // Templated
		 .RIN_P			(DOUT_P),		 // Templated
		 // Inputs
		 .INTB_IN		(INTB),			 // Templated
		 .BISTEN		(aim916_BISTEN),	 // Templated
		 .LFMODE		(aim916_LFMODE),	 // Templated
		 .MAPSEL		(aim916_MAPSEL),	 // Templated
		 .MODE_SEL		(aim916_MODE_SEL),	 // Templated
		 .OEN			(aim916_OEN),		 // Templated
		 .PDB			(aim916_PDB),		 // Templated
		 .IDx			(aim916_IDx),		 // Templated
		 .GND			(1'b0),			 // Templated
		 .VDD33_A		(1'b1),			 // Templated
		 .VDD33_B		(1'b1),			 // Templated
		 .VDDIO_A		(1'b1),			 // Templated
		 .CAPI2S		(1'b0),			 // Templated
		 .CAPLV25		(1'b0),			 // Templated
		 .CAPLV12		(1'b0),			 // Templated
		 .CAPPR12		(1'b0),			 // Templated
		 .CAPPP12		(1'b0),			 // Templated
		 .CAPL12		(1'b0),			 // Templated
		 .RES_0			(1'b0),			 // Templated
		 .RES_1			(1'b0));			 // Templated


//Behavior code for now

always @(*) begin
    aim916_TXCLKOUT_N <= #`V_DLY aim915_RXCLKIN_N;
    aim916_TXCLKOUT_P <= #`V_DLY aim915_RXCLKIN_P; 
    aim916_TXOUT_0_N <= #`V_DLY aim915_RXIN_0_N;
    aim916_TXOUT_0_P <= #`V_DLY aim915_RXIN_0_P; 
    aim916_TXOUT_1_N <= #`V_DLY aim915_RXIN_1_N; 
    aim916_TXOUT_1_P <= #`V_DLY aim915_RXIN_1_P; 
    aim916_TXOUT_2_N <= #`V_DLY aim915_RXIN_2_N; 
    aim916_TXOUT_2_P <= #`V_DLY aim915_RXIN_2_P; 
    aim916_TXOUT_3_N <= #`V_DLY aim915_RXIN_3_N; 
    aim916_TXOUT_3_P <= #`V_DLY aim915_RXIN_3_P; 
end

always @(*) begin
    aim916_I2S_DA  <= #`A_DLY aim915_I2S_DA; 
    aim916_I2S_DB  <= #`A_DLY aim915_I2S_DB; 
    aim916_I2S_DC  <= #`A_DLY aim915_I2S_DC; 
    aim916_I2S_DD  <= #`A_DLY aim915_I2S_DD;
    aim916_MCLK    <= #`A_DLY aim915_I2S_CLK; 
    aim916_I2S_CLK <= #`A_DLY aim915_I2S_CLK;
    aim916_I2S_WC  <= #`A_DLY aim915_I2S_WC; 
end
endmodule

