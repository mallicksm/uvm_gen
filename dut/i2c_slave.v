//-----------------------------------------------------------------------
// File name   : i2c_slave.v
// Author      : Zhang Yufei (Full Name Please)
// Created     : Thu 04 Jan 2018 01:09:49 AM HKT
// Description :
// Notes       :
//-----------------------------------------------------------------------
// Copyright 2015 (c) YfZhang
//-----------------------------------------------------------------------
module i2c_slave
#(
  parameter DEV_ADDR = 7'h50
 )
(
    input rstn,//async reset
    input clk, //oversample clock

    input [6:0] slave_addr,
    input write_protect, //active high

    input  scl,
    output reg scl_oen, //active low;
    input  sda,
    output reg sda_oen, //active low; sda is open drain IO, output always tie to 0, PAD side pull up

    output read_err,

    output reg psel,
    output reg penable,
    output reg pwrite,
    output reg [7:0] paddr,
    output reg [7:0] pwdata,
    input  [7:0] prdata,
    input  pready
);

reg [4:0] scl_d;
reg       scl_sync;
reg [8:0] scl_sync_d;
wire      scl_pos;
wire      scl_neg;
reg [4:0] sda_d;
reg       sda_sync;
reg [8:0] sda_sync_d;
wire      sda_pos;
wire      sda_neg;
wire      start_flag;
wire      stop_flag;
reg [2:0] bit_cnt;
wire      byte_flag;
reg [3:0] cs;
reg [3:0] ns;
reg [7:0] i2c_byte;
reg [7:0] offset;
reg       sda_oen_new;
reg       sda_oen_stall;
reg [1:0] scl_pre;
reg [1:0] sda_pre;

parameter IIC_IDLE    = 4'b0000;
parameter IIC_DEV     = 4'b0001;
parameter IIC_DEV_ACK = 4'b0010;
parameter IIC_ADR     = 4'b0011;
parameter IIC_ADR_ACK = 4'b0100;
parameter IIC_WRITE   = 4'b0101;
parameter IIC_WR_ACK  = 4'b0110;
parameter IIC_READ    = 4'b0111;
parameter IIC_RD_ACK  = 4'b1000;
parameter IIC_WAIT_ACK= 4'b1001;

always@(posedge clk or negedge rstn)
    if(!rstn)
        scl_pre <= 2'b11;
    else
        scl_pre <= {scl_pre[0], scl};

always@(posedge clk or negedge rstn)
    if(!rstn)
        scl_d <= 5'b11111;
    else
        scl_d <= {scl_d[3:0], scl_pre[1]};

always@(posedge clk or negedge rstn)
    if(!rstn)
        scl_sync <= 1'b1;
    else if(scl_d[4] + scl_d[3] + scl_d[2] + scl_d[1] + scl_d[0] >= 3'd4)
        scl_sync <= 1'b1;
    else if(scl_d[4] + scl_d[3] + scl_d[2] + scl_d[1] + scl_d[0] <= 3'd1)
        scl_sync <= 1'b0;

always@(posedge clk or negedge rstn)
    if(!rstn)
        scl_sync_d <= 9'b11111111;
    else
        scl_sync_d <= {scl_sync_d[7:0], scl_sync};

assign scl_pos =  scl_sync      && !scl_sync_d[0];
assign scl_neg = !scl_sync_d[7] &&  scl_sync_d[8];


always@(posedge clk or negedge rstn)
    if(!rstn)
        sda_pre <= 2'b11;
    else
        sda_pre <= {sda_pre[0], sda};

always@(posedge clk or negedge rstn)
    if(!rstn)
        sda_d <= 5'b11111;
    else
        sda_d <= {sda_d[3:0], sda_pre[1]};

always@(posedge clk or negedge rstn)
    if(!rstn)
        sda_sync <= 1'b1;
    else if(sda_d[4] + sda_d[3] + sda_d[2] + sda_d[1] + sda_d[0] >= 3'd4)
        sda_sync <= 1'b1;
    else if(sda_d[4] + sda_d[3] + sda_d[2] + sda_d[1] + sda_d[0] <= 3'd1)
        sda_sync <= 1'b0;

always@(posedge clk or negedge rstn)
    if(!rstn)
        sda_sync_d <= 9'b11111111;
    else
        sda_sync_d <= {sda_sync_d[7:0], sda_sync};

assign sda_pos =  sda_sync      && !sda_sync_d[0];
assign sda_neg = !sda_sync      &&  sda_sync_d[0];

assign start_flag = sda_neg && scl_sync;
assign stop_flag  = sda_pos && scl_sync;

always@(posedge clk or negedge rstn)
    if(!rstn)
        bit_cnt <= 3'd7;
    else if(start_flag || stop_flag) //start or stop
        bit_cnt <= 3'd7;
    else if(scl_pos)
        case(cs)
            IIC_DEV_ACK,
            IIC_ADR_ACK,
            IIC_WR_ACK,
            IIC_RD_ACK,
            IIC_WAIT_ACK: bit_cnt <= 3'd7;
            default: bit_cnt <= bit_cnt - 3'd1;
        endcase

assign byte_flag = scl_pos && (bit_cnt == 3'd0);

always@(posedge clk or negedge rstn)
    if(!rstn)
        cs <= IIC_IDLE;
    else
        cs <= ns;

always@(*) begin
    ns = cs;
    if(start_flag)
        ns = IIC_DEV;
    else if(stop_flag)
        ns = IIC_IDLE;
    else
        case(cs)
            IIC_DEV:   if(byte_flag)
                           //if(i2c_byte[7:1] != DEV_ADDR)
                           if(i2c_byte[7:1] != slave_addr)
                               ns = IIC_IDLE;
                           else if(sda_sync)  //write/read flag
                               ns = IIC_RD_ACK;
                           else
                               ns = IIC_DEV_ACK;
            IIC_DEV_ACK: if(scl_pos)
                             ns = IIC_ADR;
            IIC_ADR:     if(byte_flag)
                             ns = IIC_ADR_ACK;
            IIC_ADR_ACK: if(scl_pos)
                             ns = IIC_WRITE;
            IIC_WRITE:   if(write_protect)
                             ns = IIC_IDLE;
                         else if(byte_flag)
                             ns = IIC_WR_ACK;
            IIC_WR_ACK:  if(scl_pos)
                             ns = IIC_WRITE;
            IIC_RD_ACK:  if(scl_pos)
                             ns = IIC_READ;
            IIC_READ  :  if(scl_pos && (sda_sync != sda_oen))
                             ns = IIC_IDLE;
                         else if(byte_flag)
                             ns = IIC_WAIT_ACK;
            IIC_WAIT_ACK : if(penable && pready)
                               ns = IIC_READ;
                           else if(scl_pos && sda_sync)
                               ns = IIC_IDLE;
            default begin end
        endcase
end

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        i2c_byte <= 8'b0;
    end
    else begin
        case(cs)
            IIC_DEV,
            IIC_ADR,
            IIC_WRITE: if(scl_pos)
                           i2c_byte[bit_cnt] <= sda_sync;
            IIC_RD_ACK,
            IIC_WAIT_ACK: if(penable && pready)
                              i2c_byte <= prdata;
            default begin end
        endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        offset <= 8'b0;
    end
    else begin
        case(cs)
            IIC_ADR_ACK  : if(scl_pos) offset <= i2c_byte;
            IIC_WR_ACK   : if(scl_pos) offset <= offset + 7'b1;
            IIC_RD_ACK   : if(scl_neg) offset <= offset + 7'b1; //fix the burst read bug
            //IIC_WAIT_ACK : if(scl_pos) offset <= offset + 7'b1;
            IIC_WAIT_ACK : if(scl_pos & !sda_sync) offset <= offset + 7'b1; //fix the burst read bug
            default begin end
        endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        sda_oen_new <= 1'b1;
    end
    else begin
        case(ns)
            IIC_READ: sda_oen_new <= i2c_byte[bit_cnt]; //read data
            IIC_DEV_ACK,
            IIC_ADR_ACK,
            IIC_WR_ACK,
            IIC_RD_ACK: sda_oen_new <= 1'b0;
            default: sda_oen_new <= 1'b1;
        endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        sda_oen_stall <= 1'b1;
    end
    else if(scl_neg) begin
        sda_oen_stall <= sda_oen_new;
    end

//assign sda_oen = scl_sync ? sda_oen_stall : sda_oen_new;
always@(posedge clk or negedge rstn)
    if(!rstn)
        sda_oen <= 1'b1;
    else if(scl_neg)
        sda_oen <= sda_oen_new;
    else if(scl_pos)
        sda_oen <= sda_oen_stall;
     

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        scl_oen <= 1'b1;
    end
    else begin
        case(cs)
            IIC_WR_ACK,
            IIC_RD_ACK: if(scl_neg)
                            scl_oen <= 1'b0;
                        else if(penable && pready)
                            scl_oen <= 1'b1;
            IIC_WAIT_ACK: if(penable)
                              if(pready)
                                  scl_oen <= 1'b1;
                              else if(scl_neg)
                                  scl_oen <= 1'b0;
            default begin end
        endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn) begin
        psel   <= 1'b0;
        pwrite <= 1'b0;
        paddr  <= 8'b0;
        pwdata <= 8'b0;
    end
    else if(penable && pready) begin
        psel   <= 1'b0;
        pwrite <= 1'b0;
        paddr  <= 8'b0;
        pwdata <= 8'b0;
    end
    else begin
        case(cs)
            IIC_WR_ACK: if(scl_neg) begin
                              psel   <= 1'b1;
                              pwrite <= 1'b1;
                              paddr  <= offset;
                              pwdata <= i2c_byte;
                        end
            IIC_RD_ACK: if(scl_neg) begin
                              psel   <= 1'b1;
                              pwrite <= 1'b0;
                              paddr  <= offset;
                          end
            IIC_WAIT_ACK: if(scl_pos && !sda_sync) begin
                              psel   <= 1'b1;
                              pwrite <= 1'b0;
                              paddr  <= offset;
                          end
            default begin end
        endcase
    end

always@(posedge clk or negedge rstn)
    if(!rstn)
        penable <= 1'b0;
    else if(penable && pready)
        penable <= 1'b0;
    else if(psel)
        penable <= 1'b1;

assign read_err = (cs == IIC_READ) && scl_pos && (sda_sync != sda_oen);

endmodule
