module AIM916 (/*AUTOARG*/
   // Outputs
   TXCLKOUT_N, TXCLKOUT_P, TXOUT_0_N, TXOUT_0_P, TXOUT_1_N, TXOUT_1_P,
   TXOUT_2_N, TXOUT_2_P, TXOUT_3_N, TXOUT_3_P, I2S_DA, I2S_DB, I2S_DC,
   I2S_DD, MCLK, I2S_WC, I2S_CLK, LOCK, PASS, CMLOUT_N, CMLOUT_P, DMF,
   // Inouts
   GPIO_0, GPIO_1, SCL, SDA, RIN_N, RIN_P,
   // Inputs
   INTB_IN, BISTEN, LFMODE, MAPSEL, MODE_SEL, OEN, PDB, IDx, GND,
   VDD33_A, VDD33_B, VDDIO_A, CAPI2S, CAPLV25, CAPLV12, CAPPR12,
   CAPPP12, CAPL12, RES_0, RES_1
   );

// LVDS OUTPUTS
output               TXCLKOUT_N;
output               TXCLKOUT_P;

output               TXOUT_0_N;
output               TXOUT_0_P;
output               TXOUT_1_N;
output               TXOUT_1_P;
output               TXOUT_2_N;
output               TXOUT_2_P;
output               TXOUT_3_N;
output               TXOUT_3_P;

// LVCMOS GPIO
inout                GPIO_0;
inout                GPIO_1;

// I2S
output               I2S_DA;
output               I2S_DB;
output               I2S_DC;
output               I2S_DD;

output               MCLK;
output               I2S_WC;
output               I2S_CLK;

// MISC
input               INTB_IN;
input               BISTEN;
input               LFMODE;
input               MAPSEL;
input               MODE_SEL;
input               OEN;
input               PDB;
output              LOCK;
output              PASS;

input               IDx;

// I2C
inout               SCL;
inout               SDA;

// AHDL Link 
inout               RIN_N;
inout               RIN_P;
output              CMLOUT_N;
output              CMLOUT_P;
output              DMF;

// PWR and GND
input               GND;
input               VDD33_A;
input               VDD33_B;
input               VDDIO_A;

input               CAPI2S;
input               CAPLV25;
input               CAPLV12;
input               CAPPR12;
input               CAPPP12;
input               CAPL12;

input               RES_0;
input               RES_1;

/*AUTOWIRE*/

wire oe, scl_oen, sda_oen;
pullup(scl_oen);
pullup(sda_oen);
//toplevel_th.idummy_dut_top.U_AIM916.sda_oen
// Dummy Behavior Function
assign oe = 1'b1;

//Outputs
assign   TXCLKOUT_N = 1'b0; 
assign   TXCLKOUT_P = 1'b0; 
assign   TXOUT_0_N = 1'b0; 
assign   TXOUT_0_P = 1'b0; 
assign   TXOUT_1_N = 1'b0; 
assign   TXOUT_1_P = 1'b0; 
assign   TXOUT_2_N = 1'b0; 
assign   TXOUT_2_P = 1'b0; 
assign   TXOUT_3_N = 1'b0; 
assign   TXOUT_3_P = 1'b0; 
assign   I2S_DA = 1'b0; 
assign   I2S_DB = 1'b0; 
assign   I2S_DC = 1'b0; 
assign   I2S_DD = 1'b0; 
assign   MCLK = 1'b0; 
assign   I2S_WC = 1'b0; 
assign   I2S_CLK = 1'b0; 
assign   LOCK = 1'b0; 
assign   PASS = 1'b0; 
assign   CMLOUT_N = 1'b0; 
assign   CMLOUT_P = 1'b0; 
assign   DMF = 1'b0; 

//Inouts
assign    GPIO_0 = oe ? 1'b0 : 1'bz; 
assign    GPIO_1 = oe ? 1'b0 : 1'bz; 
assign    SCL = scl_oen ? 1'bz : 1'b0; 
assign    SDA = sda_oen ? 1'bz : 1'b0; 
assign    RIN_N = oe ? 1'b0 : 1'bz; 
assign    RIN_P = oe ? 1'b0 : 1'bz; 

endmodule
