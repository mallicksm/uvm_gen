################################################################################
# UVC Configuration
################################################################################
agent_has_env  = yes
uvm_seqr_class = yes

################################################################################
# Agent Settings
################################################################################
agent_name = OpenLDI_tx
trans_item = pixel_output
agent_is_active = UVM_PASSIVE

################################################################################
# Transaction Class Configuration
################################################################################
trans_var  = rand byte red;
trans_var  = rand byte blue;
trans_var  = rand byte green;
trans_var  = rand bit DE;
trans_var  = rand bit VS;
trans_var  = rand bit HS;
config_var = int OpenLDI_pll_lock_count = 2;

################################################################################
# Driver Include
################################################################################
monitor_inc             = OpenLDI/tx_do_mon.sv inline
if_inc_inside_interface = OpenLDI/tx_if_cb.sv  inline

################################################################################
# Interface Configuration
################################################################################
if_port    = logic lvds_clock;
if_port    = logic reset;
if_port    = logic CLK_P;
if_port    = logic DAT_0_P;
if_port    = logic DAT_1_P;
if_port    = logic DAT_2_P;
if_port    = logic DAT_3_P;
if_clock   = lvds_clock
if_reset   = reset
