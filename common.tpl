################################################################################
# Configuration Parameters
################################################################################
project               = verif
dut_top               = dummy_dut_top
dut_iname             = idummy_dut_top
dut_pfile             = pinlist
dut_path              = include
dut_source_path       = dut
backup                = no
nested_config_objects = yes
dual_top              = yes

################################################################################
# Generation Recipe
################################################################################
test_set_drain_time           = 50000
th_inc_inside_module          = th_inc_clk.sv inline
th_generate_clock_and_reset   = no
comments_at_include_locations = no
top_default_seq_count         = 10
timeunit                      = 1ns
timeprecision                 = 10ps

################################################################################
# Reference Model
################################################################################
syosil_scoreboard_src_path = syosil/src
# OpenLDI Scoreboard
ref_model_input            = ODI_reference m_OpenLDI_rx_env.m_OpenLDI_rx_agent
ref_model_output           = ODI_reference m_OpenLDI_tx_env.m_OpenLDI_tx_agent
ref_model_compare_method   = ODI_reference iop
ref_model_inc_inside_class = ODI_reference OpenLDI_ref_model_inc_inside_class.sv inline
ref_model_inc_after_class  = ODI_reference OpenLDI_ref_model_inc_after_class.sv  inline
# I2S Scoreboard
ref_model_input            = I2S_reference m_I2S_rx_env.m_I2S_rx_agent
ref_model_output           = I2S_reference m_I2S_tx_env.m_I2S_tx_agent
ref_model_compare_method   = I2S_reference iop
ref_model_inc_inside_class = I2S_reference i2s_ref_model_inc_inside_class.sv inline
ref_model_inc_after_class  = I2S_reference i2s_ref_model_inc_after_class.sv  inline
# Insert initial config code in toplevel_tb.sv
tb_inc_before_run_test     = toplevel_tb_config.sv inline
# Insert alt test
test_inc_after_class       = alt_test.sv inline
################################################################################
# Header Information
################################################################################
file_header_inc = header.txt
copyright       = Copyright Microsemi Inc (c) 2018
name            = Soummya Mallick
company         = Microsemi Inc
tel             = (916)846-4335
email           = soummya.mallick@microsemi.com
dept            = Digital Design
year            = 2018
version         = 6.0
