agent_name = data_output

number_of_instances = 4

trans_item = output_tx
trans_var  = rand byte addr;
trans_var  = rand byte data;

agent_is_active = uvm_passive

agent_coverage_enable = no

monitor_inc = data_output_do_mon.sv inline

if_port  = logic ena;
if_port  = byte addr;
if_port  = byte data;
if_port  = logic clk;
if_port  = logic reset;
if_clock = clk
if_reset = reset
