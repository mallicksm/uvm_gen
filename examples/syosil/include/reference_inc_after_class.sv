function void reference::write_reference_0(input_tx t);
  send(t);
endfunction
  
function void reference::write_reference_1(input_tx t);
  send(t);
endfunction
  
function void reference::write_reference_2(input_tx t);
  send(t);
endfunction
  
function void reference::write_reference_3(input_tx t);
  send(t);
endfunction
  
function void reference::send(input_tx t);
  output_tx tx;
  tx = output_tx::type_id::create("tx");
  tx.addr = t.addr;
  tx.data = t.data;
  case (t.addr % 4)
  0: analysis_port_0.write(tx);
  1: analysis_port_1.write(tx);
  2: analysis_port_2.write(tx);
  3: analysis_port_3.write(tx);
  endcase
  `uvm_info(get_type_name(), $sformatf("addr = %0d, data = %0d", t.addr, t.data), UVM_HIGH)
endfunction
  
