task data_output_monitor::do_mon;
  forever @(posedge vif.clk)
  begin
    if (vif.ena)
    begin
      m_trans.addr = vif.addr;
      m_trans.data = vif.data;
      analysis_port.write(m_trans);
      `uvm_info(get_type_name(), $sformatf("Output addr = %0d, data = %0d", m_trans.addr, m_trans.data), UVM_HIGH)
    end
  end
endtask
