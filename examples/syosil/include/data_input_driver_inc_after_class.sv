task data_input_driver::run_phase(uvm_phase phase);
  `uvm_info(get_type_name(), "run_phase", UVM_HIGH)

  forever
  begin
    seq_item_port.get_next_item(req);

    wait (vif.reset == 1);
    phase.raise_objection(this);
  
    repeat ($urandom_range(0, 1)) @(posedge vif.clk);

    vif.addr <= req.addr;
    vif.data <= req.data;
    vif.ena  <= 1;
    @(posedge vif.clk);
    vif.ena  <= 0;
  
    fork
      begin
        repeat (10) @(posedge vif.clk);
        phase.drop_objection(this);
      end
    join_none
    seq_item_port.item_done();
  end
endtask : run_phase
