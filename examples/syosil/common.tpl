dut_top               = switch
nested_config_objects = yes

syosil_scoreboard_src_path = ../../syosil/src

ref_model_input  = reference m_data_input_0_agent
ref_model_input  = reference m_data_input_1_agent
ref_model_input  = reference m_data_input_2_agent
ref_model_input  = reference m_data_input_3_agent

ref_model_output = reference m_data_output_0_agent
ref_model_output = reference m_data_output_1_agent
ref_model_output = reference m_data_output_2_agent
ref_model_output = reference m_data_output_3_agent

ref_model_compare_method    = reference iop

ref_model_inc_inside_class  = reference reference_inc_inside_class.sv  inline
ref_model_inc_after_class   = reference reference_inc_after_class.sv   inline

top_default_seq_count = 100

#uvm_cmdline = +UVM_VERBOSITY=UVM_HIGH
