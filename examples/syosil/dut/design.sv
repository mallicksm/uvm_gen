// This needs to contain the top-level of the DUT
// or it can be left blank

module switch (
  input logic  clk,
  input logic  reset,
  
  input logic  ena_ia,
  input byte   addr_ia,
  input byte   data_ia,

  input logic  ena_ib,
  input byte   addr_ib,
  input byte   data_ib,

  input logic  ena_ic,
  input byte   addr_ic,
  input byte   data_ic,

  input logic  ena_id,
  input byte   addr_id,
  input byte   data_id,

  output logic ena_oa,
  output byte  addr_oa,
  output byte  data_oa,

  output logic ena_ob,
  output byte  addr_ob,
  output byte  data_ob,

  output logic ena_oc,
  output byte  addr_oc,
  output byte  data_oc,

  output logic ena_od,
  output byte  addr_od,
  output byte  data_od  
  );
  
  byte data_q[$];
  byte addr_q[$];
  
  bit busy_oa;
  bit busy_ob;
  bit busy_oc;
  bit busy_od;
  
  always @(posedge clk)
  begin
    if (!reset)
    begin
      busy_oa = 0;
      busy_ob = 0;
      busy_oc = 0;
      busy_od = 0;
      ena_oa <= 0;
      ena_ob <= 0;
      ena_oc <= 0;
      ena_od <= 0;
    end
    else
    begin
      if (busy_oa)
      begin
        busy_oa = 0;
        ena_oa <= 0;
        addr_oa <= 0;
        data_oa <= 0;
      end
    
      if (busy_ob)
      begin
        busy_ob = 0;
        ena_ob <= 0;
        addr_ob <= 0;
        data_ob <= 0;
      end
    
      if (busy_oc)
      begin
        busy_oc = 0;
        ena_oc <= 0;
        addr_oc <= 0;
        data_oc <= 0;
      end
    
      if (busy_od)
      begin
        busy_od = 0;
        ena_od <= 0;
        addr_od <= 0;
        data_od <= 0;
      end
    
      if (ena_ia)
      begin
        data_q.push_back(data_ia);
        addr_q.push_back(addr_ia);
      end  

      if (ena_ib)
      begin
        data_q.push_back(data_ib);
        addr_q.push_back(addr_ib);
      end  

      if (ena_ic)
      begin
        data_q.push_back(data_ic);
        addr_q.push_back(addr_ic);
      end  

      if (ena_id)
      begin
        data_q.push_back(data_id);
        addr_q.push_back(addr_id);
      end
    
      for (int i = 0; i < addr_q.size; i++)
      begin
        case (addr_q[i] % 4)
        0: if (!busy_oa)
           begin
             busy_oa = 1;
             ena_oa <= 1;
             addr_oa <= addr_q[i];
             data_oa <= data_q[i];
             addr_q.delete(i);
             data_q.delete(i);
             i--;
           end

        1: if (!busy_ob)
           begin
             busy_ob = 1;
             ena_ob <= 1;
             addr_ob <= addr_q[i];
             data_ob <= data_q[i];
             addr_q.delete(i);
             data_q.delete(i);
             i--;
           end

        2: if (!busy_oc)
           begin
             busy_oc = 1;
             ena_oc <= 1;
             addr_oc <= addr_q[i];
             data_oc <= data_q[i];
             addr_q.delete(i);
             data_q.delete(i);
             i--;
           end

        3: if (!busy_od)
           begin
             busy_od = 1;
             ena_od <= 1;
             addr_od <= addr_q[i];
             data_od <= data_q[i];
             addr_q.delete(i);
             data_q.delete(i);
             i--;
           end
        endcase
      end
    end
  end
  
endmodule
