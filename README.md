# README #

git clone https://bitbucket.org/mallicksm/uvm_gen

### What is this repository for? ###

* Quick summary
Quickly generate a uvm testbench that is based out of ref_flow as described
in accellera.org
* Version
1.0

### How do I get set up? ###

* Summary of set up
just run make
* Configuration
Place uvm_gen in your bin directory and install perl
* Dependencies
yum install perl
yum install perl-File-Copy-Recursive
* How to run tests
make

### Who do I talk to? ###

* Repo owner or admin
Soummya Mallick
