// Example code for alternative sequence
class I2C_915_alt_seq extends I2C_915_default_seq;

  `uvm_object_utils(I2C_915_alt_seq)

  extern function new(string name = "");
  extern task body();

endclass : I2C_915_alt_seq


function I2C_915_alt_seq::new(string name = "");
  super.new(name);
endfunction : new


task I2C_915_alt_seq::body();
  `uvm_info(get_type_name(), "Default sequence starting", UVM_HIGH)

// Sending 2 sequences, first with write address=a0 then with read address=a0
// Targetting the 915 slave
  `uvm_do_with(req,
  {
     req.address == 'h50;
     req.offset == 'h10;
     req.rd_wrn == 'b0;
     req.burst_size == 4;
  })

  `uvm_do_with(req,
  {
     req.address == 'h50;
     req.offset == 'h10;
     req.rd_wrn == 'b1;
     req.burst_size == 4;
  })

  `uvm_info(get_type_name(), "Default sequence completed", UVM_HIGH)
endtask : body

