  bit bus_idle_flag = 1;
  event stop_event;
  event start_event;

  extern task run_phase(uvm_phase phase);
  extern virtual task reset_signals();
  extern virtual task drive_trans(ref i2c_trans tr);
  extern virtual task monitor_bus_idle();
//##############################################################################
// High Level Bus functions (byte wide)
//##############################################################################
  extern virtual task drive_byte(input bit [7:0] data, output bit ack);
  extern virtual task read_byte(output bit [7:0] data, input bit ack);
//##############################################################################
// Primitive bus functions
//##############################################################################
  extern virtual task drive_start();
  extern virtual task drive_stop();
  extern virtual task drive_bit(input bit data);
  extern virtual task read_bit(output bit data);
