task I2C_915_monitor::run_phase(uvm_phase phase);
  `uvm_info(get_type_name(), "run_phase", UVM_HIGH)
  reset_signals();
  fork
    monitor_bus_idle;
    forever begin
       monitor_bus();
    end
  join_any
endtask : run_phase

task I2C_915_monitor::reset_signals();
  wait(vif.rstn == 1);
  vif.scl_slv_oen = 1;
  vif.sda_slv_oen = 1;
endtask:reset_signals

task I2C_915_monitor::monitor_bus();
    int idx = 0;
    wait(bus_idle_flag == 0);
    read_byte({address[6:0],rd_wrn}, 1'b0);
    if(address[6:0] == slave_address[7:1]) begin
       read_byte(offset, 1'b0);
       read_byte(data, 1'b0);             // restart for read, data for write
       if (restart_event.triggered) begin // Read Requested
          read_byte({address[6:0],rd_wrn}, 1'b0);
          ack = 0;
          while (ack == 0) begin
             data = slave_mem[offset[7:0] + idx];
             drive_byte(data, ack);
             idx++;
          end
          wait(bus_idle_flag == 1);
       end else begin                     // write Requested
          slave_mem[offset[7:0] + idx] = data;
          while (!stop_event.triggered) begin
             idx++;
             read_byte(data, 1'b0);
             if (!stop_event.triggered) slave_mem[offset[7:0] + idx] = data;
          end
       end
    end else begin
       wait(bus_idle_flag == 1);
    end
endtask:monitor_bus

task I2C_915_monitor::monitor_bus_idle();
  fork
    forever @(stop_event)  bus_idle_flag = 1;
    forever @(start_event) bus_idle_flag = 0;
    forever @(posedge vif.sda iff vif.scl) -> stop_event;
    forever @(negedge vif.sda iff vif.scl) -> start_event;
    forever @(negedge vif.sda iff (vif.scl && !bus_idle_flag)) -> restart_event;
  join
endtask:monitor_bus_idle

//##############################################################################
// High Level Bus functions (byte wide)
//##############################################################################
// Passive drive_byte implementation for slave i2c interface
task I2C_915_monitor::drive_byte(input bit [7:0] data, output bit ack);
   bit early_exit = 0;
  `uvm_info("DRV_BYTE", $sformatf("I2C drive byte %02h", data), UVM_HIGH);
   for(int i = 7; i>=0; i--) begin
      drive_bit(data[i]);
   end
   read_bit(ack);
endtask:drive_byte

// Passive read_byte implementation for slave i2c interface
task I2C_915_monitor::read_byte(output bit [7:0] data, input bit ack);
   bit early_exit = 0;
   for(int i = 7; i>=0; i--) begin
      fork 
         read_bit(data[i]);
         @(restart_event or stop_event) begin 
            early_exit = 1;
         end
      join_any
      disable fork;
      if (early_exit) begin
         break;
      end
   end
     `uvm_info("READ_BYTE", $sformatf("I2C read byte %02h", data), UVM_HIGH);
  if (!early_exit) drive_bit(ack);
endtask:read_byte

//##############################################################################
// Primitive bus functions
//##############################################################################
// Passive drive_bit implementation for slave i2c interface
task I2C_915_monitor::drive_bit(bit data);
  `uvm_info("SLAVE DRV_BIT", $sformatf("I2C drive bit %b", data), UVM_DEBUG);
  @(negedge vif.scl);
  #(m_config.thddat);
  vif.sda_slv_oen = data;
  @(posedge vif.scl);
  #(m_config.thigh);
  fork begin
     @(negedge vif.scl) #550 vif.sda_slv_oen = 1;
  end
  join_none
endtask:drive_bit

// Passive read_bit implementation for slave i2c interface
task I2C_915_monitor::read_bit(output bit data);
   @(negedge vif.scl) #(m_config.thddat) vif.sda_slv_oen = 1;
   @(posedge vif.scl) data = vif.sda;
   #(m_config.thigh);
  `uvm_info("SLAVE READ_BIT", $sformatf("I2C read bit %b", data), UVM_DEBUG);
endtask:read_bit
