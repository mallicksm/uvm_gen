clocking aim_monitor @(posedge lvds_clock);
  default input #1step;
  input CLK_P;
  input DAT_0_P;
  input DAT_1_P;
  input DAT_2_P;
  input DAT_3_P;
endclocking: aim_monitor
