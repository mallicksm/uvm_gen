clocking aim_driver @(posedge lvds_clock);
  default output #1;
  output CLK_P;
  output CLK_N;
  output DAT_0_P;
  output DAT_0_N;
  output DAT_1_P;
  output DAT_1_N;
  output DAT_2_P;
  output DAT_2_N;
  output DAT_3_P;
  output DAT_3_N;
endclocking: aim_driver

clocking aim_monitor @(posedge lvds_clock);
  default input #1step;
  input CLK_P;
  input DAT_0_P;
  input DAT_1_P;
  input DAT_2_P;
  input DAT_3_P;
endclocking: aim_monitor
