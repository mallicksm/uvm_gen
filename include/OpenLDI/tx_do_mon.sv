task OpenLDI_tx_monitor::do_mon();
   wait (vif.reset == 1);
   @(negedge vif.aim_monitor.CLK_P);
   @(posedge vif.aim_monitor.CLK_P);
   @(vif.aim_monitor);
   @(vif.aim_monitor);
   forever begin
      m_trans.green[2] = vif.aim_monitor.DAT_0_P;
      m_trans.blue[3]  = vif.aim_monitor.DAT_1_P;
      m_trans.DE       = vif.aim_monitor.DAT_2_P;
      @(vif.aim_monitor)
      m_trans.red[7]   = vif.aim_monitor.DAT_0_P;
      m_trans.blue[2]  = vif.aim_monitor.DAT_1_P;
      m_trans.VS       = vif.aim_monitor.DAT_2_P;
      m_trans.blue[1]  = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor)
      wait (vif.aim_monitor.CLK_P == 0);
      m_trans.red[6]   = vif.aim_monitor.DAT_0_P;
      m_trans.green[7] = vif.aim_monitor.DAT_1_P;
      m_trans.HS       = vif.aim_monitor.DAT_2_P;
      m_trans.blue[0]  = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor)
      m_trans.red[5]   = vif.aim_monitor.DAT_0_P;
      m_trans.green[6] = vif.aim_monitor.DAT_1_P;
      m_trans.blue[7]  = vif.aim_monitor.DAT_2_P;
      m_trans.green[1] = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor)
      m_trans.red[4]   = vif.aim_monitor.DAT_0_P;
      m_trans.green[5] = vif.aim_monitor.DAT_1_P;
      m_trans.blue[6]  = vif.aim_monitor.DAT_2_P;
      m_trans.green[0] = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor)
      wait (vif.aim_monitor.CLK_P == 1);
      m_trans.red[3]   = vif.aim_monitor.DAT_0_P;
      m_trans.green[4] = vif.aim_monitor.DAT_1_P;
      m_trans.blue[5]  = vif.aim_monitor.DAT_2_P;
      m_trans.red[1]   = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor)
      m_trans.red[2]   = vif.aim_monitor.DAT_0_P;
      m_trans.green[3] = vif.aim_monitor.DAT_1_P;
      m_trans.blue[4]  = vif.aim_monitor.DAT_2_P;
      m_trans.red[0]   = vif.aim_monitor.DAT_3_P;
      @(vif.aim_monitor);
      if ((m_trans.red != 0) || (m_trans.green != 0) || (m_trans.blue == 0)) begin
         if (m_config.OpenLDI_pll_lock_count <= 0) begin
           analysis_port.write(m_trans);
           `uvm_info(get_type_name(), $sformatf("\nODI tx: red=%h blue=%h green=%h DE=%d VS=%d HS=%d",m_trans.red,m_trans.blue,m_trans.green,m_trans.DE,m_trans.VS,m_trans.HS), UVM_MEDIUM)
         end else begin
            m_config.OpenLDI_pll_lock_count--;
         end
      end
   end
endtask
