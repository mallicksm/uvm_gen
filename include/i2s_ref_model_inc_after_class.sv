function void I2S_reference::write_I2S_reference_0(audio_input t);
  send(t);
endfunction
function void I2S_reference::send(audio_input t);
  audio_output sample;
  sample = audio_output::type_id::create("sample");
  sample.audio_data_chA = t.audio_data_chA;
  sample.audio_data_chB = t.audio_data_chB;
  sample.audio_data_chC = t.audio_data_chC;
  sample.audio_data_chD = t.audio_data_chD;
  analysis_port_0.write(sample);
endfunction
