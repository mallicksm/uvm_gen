class alt_test extends toplevel_test;

  `uvm_component_utils(alt_test)


  extern function new(string name, uvm_component parent);
  extern function void start_of_simulation_phase (uvm_phase phase);

endclass : alt_test

function alt_test::new(string name, uvm_component parent);
  super.new(name, parent);
endfunction : new

function void alt_test::start_of_simulation_phase (uvm_phase phase);
  I2C_915_default_seq::type_id::set_type_override( I2C_915_alt_seq::get_type());
endfunction: start_of_simulation_phase
