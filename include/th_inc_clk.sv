// Clk & Reset
  logic lvds_clock = 0;
  logic i2s_clock = 0;
  logic reset = 0;

  always #100 lvds_clock = ~lvds_clock; // pclk = 5Mhz
  always #5 i2s_clock = ~i2s_clock;     // i2s_clock = 1Mhz

  initial #75 reset = 1;

// Drive Interfaces
  assign I2S_rx_if_0.reset          = reset;
  assign I2S_tx_if_0.reset          = reset;
  assign OpenLDI_rx_if_0.reset      = reset;
  assign OpenLDI_tx_if_0.reset      = reset;
  assign I2C_915_if_0.rstn          =  idummy_dut_top.U_AIM915.RESETN;
  assign I2C_916_if_0.rstn          =  idummy_dut_top.U_AIM915.RESETN;

  assign I2S_rx_if_0.I2S_CLK        = i2s_clock;
  assign OpenLDI_rx_if_0.lvds_clock = lvds_clock;
  assign OpenLDI_tx_if_0.lvds_clock = lvds_clock;
