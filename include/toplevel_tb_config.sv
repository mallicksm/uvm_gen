// Example include additional config
// The lock count deletes additional transactions per interface, currently set to 0
top_env_config.m_I2S_rx_config.I2S_pll_lock_count = 0;
top_env_config.m_I2S_tx_config.I2S_pll_lock_count = 0;
top_env_config.m_OpenLDI_rx_config.OpenLDI_pll_lock_count = 0;
top_env_config.m_OpenLDI_tx_config.OpenLDI_pll_lock_count = 0;
