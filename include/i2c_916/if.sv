logic scl_oen;
logic sda_oen;
logic scl_slv_oen;
logic sda_slv_oen;
logic rstn;

assign scl = (scl_oen && scl_slv_oen) ? 1'bz : 1'b0; // Active Low
assign sda = (sda_oen && sda_slv_oen) ? 1'bz : 1'b0; // Active Low
