task I2C_916_driver::run_phase(uvm_phase phase);
  `uvm_info(get_type_name(), "run_phase", UVM_HIGH)
  reset_signals();
  fork
     monitor_bus_idle;
     #1000ns;
     forever begin
       seq_item_port.get_next_item(req);
       drive_trans(req);
       req.print;
       $cast(rsp, req.clone());
       rsp.set_id_info(req);
       seq_item_port.item_done(rsp);
     end
  join_none
endtask:run_phase

task I2C_916_driver::reset_signals();
  wait(vif.rstn == 1);
  vif.scl_oen = 1;
  vif.sda_oen = 1;
endtask:reset_signals

task I2C_916_driver::drive_trans(ref i2c_trans tr);
    wait(bus_idle_flag);
    #(m_config.tbuf);

    if(tr.rd_wrn == 0) begin
       // Write Transaction
       drive_start;
       drive_byte({tr.address[6:0],1'b0}, tr.ack);
       drive_byte(tr.offset, tr.ack);
       for(int i = 0; i < tr.burst_size; ++i) begin
         drive_byte(tr.data[i], tr.ack);
       end
       drive_stop;
    end else begin
       // Read Transaction
       drive_start;
       drive_byte({tr.address[6:0],1'b0}, tr.ack);
       drive_byte(tr.offset, tr.ack);
       drive_start; // restart
       drive_byte({tr.address[6:0],1'b1}, tr.ack);
       foreach( tr.data[i] ) begin
          tr.data[i] = 0;
       end
       for(int i = 0; i < tr.burst_size-1; ++i) begin
         read_byte(tr.data[i], 1'b0); 
       end
       read_byte(tr.data[tr.burst_size-1], 1'b1); 
       drive_stop;
    end

endtask:drive_trans

task I2C_916_driver::monitor_bus_idle();
  fork
    forever @(stop_event)  bus_idle_flag = 1;
    forever @(start_event) bus_idle_flag = 0;
    forever @(posedge vif.sda iff vif.scl) -> stop_event;
    forever @(negedge vif.sda iff vif.scl) -> start_event;
  join
endtask:monitor_bus_idle

//##############################################################################
// High Level Bus functions (byte wide)
//##############################################################################
task I2C_916_driver::drive_byte(input bit [7:0] data, output bit ack);
  `uvm_info("DRV_BYTE", $sformatf("I2C drive byte %02h", data), UVM_HIGH);
  for(int i = 7; i>=0; i--) begin
    drive_bit(data[i]);
  end
  read_bit(ack);
endtask:drive_byte

task I2C_916_driver::read_byte(output bit [7:0] data, input bit ack);
  for(int i = 7; i>=0; i--) begin
    read_bit(data[i]);
  end
  `uvm_info("READ_BYTE", $sformatf("I2C read byte %02h", data), UVM_HIGH);
  drive_bit(ack);
endtask:read_byte

//##############################################################################
// Primitive bus functions
//##############################################################################

//******************************************************************************
// Drive Start
//                                +---------------------------------------
//  scl                           |                     
//      ----------------+---------+                    
//             600         600         1300           600         600
//          <--thigh---><--thigh-><----tbuf-----><--tsusta--><--thdsta-->
//                                             
//          +-----------------------------------------------+
//  sda     |                                               |
//      ----+                                               +------------
//******************************************************************************

task I2C_916_driver::drive_start();
  if(!vif.sda) begin
    fork
      begin
      vif.scl_oen = 0;
      vif.sda_oen = 1;
      wait(vif.sda);
      end
      #1ms `uvm_error("I2C_TIMEOUT", "Cannot monitor a IDLE state to issue a Start command");
    join_any
    disable fork;
    #(m_config.thigh); // 600
    #(m_config.thigh); // 600
    fork
      vif.scl_oen = 1;
      @(posedge vif.scl);
    join
    #(m_config.tbuf); // 1300
  end
  vif.scl_oen = 1;
  vif.sda_oen = 1;
  #(m_config.tsusta); // 600
  vif.scl_oen = 1;
  vif.sda_oen = 0;
  `uvm_info("DRV_START", "", UVM_HIGH);
  #(m_config.thdsta); // 600
endtask:drive_start

//******************************************************************************
// Drive Stop
//                   +--------------------------------------
// scl               |
//     --------------+
//          1300              600                1300
//     <----tlow-----><------tsusto--------><----tbuf------>
//     
//                                         +----------------
// sda                                     |
//     ------------------------------------+
//******************************************************************************

task I2C_916_driver::drive_stop();
  vif.scl_oen = 0;
  #(m_config.thddat);
  vif.sda_oen = 0;
  #(m_config.tlow);   // 1300
  fork
    vif.scl_oen = 1;
    @(posedge vif.scl);
  join
  #(m_config.tsusto); // 600
  vif.sda_oen = 1;
  `uvm_info("DRV_STOP", "", UVM_HIGH);
  #(m_config.tbuf);   // 1300
endtask:drive_stop

//******************************************************************************
// Drive Bit
//      ----+                     +--------------+                  +----
//  scl     |                     |              |                  |
//          +-----------+---------+              +----------+-------+
//              600        1300         600    
//          <--thddat--><--tlow--><----thigh----><--thddat-->
//                                             
//                      +-----------------------------------+
//  sda                 |                                   |
//      ----------------+                                   +------------
//
//******************************************************************************
task I2C_916_driver::drive_bit(bit data);
  `uvm_info("DRV_BIT", $sformatf("I2C drive bit %b", data), UVM_DEBUG);
  fork
  begin
     vif.scl_oen = 0;
     #(m_config.thddat);
     #(m_config.tlow);
     vif.scl_oen = 1;
  end
  join_none
  @(negedge vif.scl);
  #(m_config.thddat);
  vif.sda_oen = data;
  @(posedge vif.scl);
  #(m_config.thigh);
  fork
     #550 vif.sda_oen = 1;
  join_none
endtask:drive_bit

task I2C_916_driver::read_bit(output bit data);
  fork
  begin
     vif.scl_oen = 0;
     #(m_config.thddat);
     #(m_config.tlow);
     vif.scl_oen = 1;
  end
  join_none
  @(negedge vif.scl) #(m_config.thddat) vif.sda_oen = 1;
  @(posedge vif.scl) data = vif.sda;
  #(m_config.thigh);
  `uvm_info("READ_BIT", $sformatf("I2C read bit %b", data), UVM_DEBUG);
endtask:read_bit
