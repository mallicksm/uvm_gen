clocking aim_driver @(negedge I2S_CLK);
  default output #1;
  output I2S_WC;
  output I2S_DA;
  output I2S_DB;
  output I2S_DC;
  output I2S_DD;
endclocking: aim_driver

clocking aim_monitor @(negedge I2S_CLK);
  default input #1step;
  input I2S_WC;
  input I2S_DA;
  input I2S_DB;
  input I2S_DC;
  input I2S_DD;
endclocking: aim_monitor
