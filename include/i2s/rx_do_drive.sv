task I2S_rx_driver::do_drive();
   wait (vif.reset == 1);
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 0;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[7];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[7];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[7];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[7];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 0;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[6];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[6];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[6];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[6];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 0;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[5];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[5];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[5];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[5];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 1;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[4];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[4];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[4];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[4];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 1;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[3];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[3];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[3];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[3];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 1;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[2];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[2];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[2];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[2];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 1;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[1];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[1];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[1];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[1];
   @(vif.aim_driver);
   vif.aim_driver.I2S_WC <= 0;
   vif.aim_driver.I2S_DA <= req.audio_data_chA[0];
   vif.aim_driver.I2S_DB <= req.audio_data_chB[0];
   vif.aim_driver.I2S_DC <= req.audio_data_chC[0];
   vif.aim_driver.I2S_DD <= req.audio_data_chD[0];
endtask
