task I2S_tx_monitor::do_mon();
   wait (vif.reset == 1);
   @(posedge vif.aim_monitor.I2S_WC);
   @(negedge vif.aim_monitor.I2S_WC);
   @(vif.aim_monitor);
   forever begin
   m_trans.audio_data_chA[7] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[7] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[7] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[7] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[6] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[6] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[6] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[6] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[5] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[5] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[5] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[5] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   wait(vif.aim_monitor.I2S_WC == 1);
   m_trans.audio_data_chA[4] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[4] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[4] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[4] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[3] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[3] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[3] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[3] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[2] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[2] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[2] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[2] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[1] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[1] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[1] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[1] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   m_trans.audio_data_chA[0] = vif.aim_monitor.I2S_DA;
   m_trans.audio_data_chB[0] = vif.aim_monitor.I2S_DB;
   m_trans.audio_data_chC[0] = vif.aim_monitor.I2S_DC;
   m_trans.audio_data_chD[0] = vif.aim_monitor.I2S_DD;
   @(vif.aim_monitor)
   if (m_config.I2S_pll_lock_count <= 0) begin
     analysis_port.write(m_trans);
     `uvm_info(get_type_name(), $sformatf("\nI2S tx: da=%h db=%h dc=%h dd=%h",m_trans.audio_data_chA,m_trans.audio_data_chB,m_trans.audio_data_chC,m_trans.audio_data_chD), UVM_MEDIUM)
   end else begin
      m_config.I2S_pll_lock_count--;
   end
   end
endtask
