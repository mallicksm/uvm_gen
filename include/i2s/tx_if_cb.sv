clocking aim_monitor @(negedge I2S_CLK);
  default input #1step;
  input I2S_WC;
  input I2S_DA;
  input I2S_DB;
  input I2S_DC;
  input I2S_DD;
endclocking: aim_monitor
