function void ODI_reference::write_ODI_reference_0(pixel_input t);
  send(t);
endfunction
function void ODI_reference::send(pixel_input t);
  pixel_output pixel;
  pixel = pixel_output::type_id::create("pixel");
  pixel.red = t.red;
  pixel.blue = t.blue;
  pixel.green = t.green;
  pixel.DE = t.DE;
  pixel.VS = t.VS;
  pixel.HS = t.HS;
  analysis_port_0.write(pixel);
endfunction
