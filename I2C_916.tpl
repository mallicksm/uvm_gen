################################################################################
# UVC Configuration
################################################################################
agent_has_env  = yes
uvm_seqr_class = yes

################################################################################
# Agent Settings
################################################################################
agent_name = I2C_916
trans_item = i2c_trans
agent_is_active = UVM_ACTIVE

################################################################################
# Transaction Class Configuration
################################################################################
trans_var  = rand byte address; // 7 bit only
trans_var  = rand byte offset;
trans_var  = rand byte data[8];
trans_var  = rand bit rd_wrn;
trans_var  = rand int burst_size;
trans_var  = bit ack;
trans_var_constraint = { burst_size > 0;burst_size < 8; }

config_var = int thigh  = 600;  //min 0.6us
config_var = int tlow   = 1300; //min 1.3us
config_var = int thdsta = 600;
config_var = int tsusta = 600;
config_var = int thddat = 600;  //min 0 max 3.45us
config_var = int tsudat = 100;  //min 100ns
config_var = int tsusto = 600;
config_var = int tbuf   = 1300;
config_var = int tsp    = 50;

################################################################################
# Driver Include
################################################################################
# driver
driver_generate_methods_inside_class = no
driver_generate_methods_after_class = no
driver_inc_inside_class  = i2c_916/driver_inside_class.sv  inline
driver_inc_after_class   = i2c_916/driver_after_class.sv   inline

# monitor
monitor_inc_inside_class = i2c_916/monitor_inside_class.sv inline
monitor_inc_after_class  = i2c_916/monitor_after_class.sv  inline

# Interface
if_inc_inside_interface  = i2c_916/if.sv                   inline

################################################################################
# Interface Configuration
################################################################################
if_port    = wire scl;
if_port    = wire sda;
