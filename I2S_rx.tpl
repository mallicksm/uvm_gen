################################################################################
# UVC Configuration
################################################################################
agent_has_env  = yes
uvm_seqr_class = yes

################################################################################
# Agent Settings
################################################################################
agent_name = I2S_rx
trans_item = audio_input
agent_is_active = UVM_ACTIVE

################################################################################
# Transaction Class Configuration
################################################################################
trans_var  = rand logic [7:0] audio_data_chA;
trans_var  = rand logic [7:0] audio_data_chB;
trans_var  = rand logic [7:0] audio_data_chC;
trans_var  = rand logic [7:0] audio_data_chD;
config_var = int I2S_pll_lock_count = 2;

################################################################################
# Driver Include
################################################################################
driver_inc              = i2s/rx_do_drive.sv inline
monitor_inc             = i2s/rx_do_mon.sv   inline
if_inc_inside_interface = i2s/rx_if_cb.sv    inline

################################################################################
# Interface Configuration
################################################################################
if_port    = logic I2S_CLK;
if_port    = logic reset;
if_port    = logic I2S_WC;
if_port    = logic I2S_DA;
if_port    = logic I2S_DB;
if_port    = logic I2S_DC;
if_port    = logic I2S_DD;
if_clock   = I2S_CLK
if_reset   = reset
